## Galaxy Data Analytics

### Aims 

To detect the centre of galaxies in the image.

### Details

The ImageAnalyzer has a range of filters that can be applied. The k-means clustering algorithm is used to pinpoint the galactic centres and requires knowning how many galaxies are in the image which is an unfortunate aspect. The other filters are about speeding up the processing.

### Tech

Uses Scala, Java and Apache Spark (for k-means).

### Usage

Get a pre-defined ImageAnalyser that uses k-means:

```
val numberOfCluserPoints: Int = ????
val imageAnalyser = KMeansImageAnalyser.get()
imageAnalyser.process("./path/to/image/file", numberOfCluserPoints)
```

### Output

Image outputs are stored under:

`./data/images/altered/{image-basename}/...`

### Sample Output

* [ARP 271](./sample-output/arp271.png "ARP 271")
* [ARP 87](./sample-output/arp87.png "ARP 87")
* [ARP m31](./sample-output/m31.png "M31")
* [ARP m83](./sample-output/m83.png "M83")


## Next steps

### Determine galactic classification

There are three basic classifications:

* Elliptical
* Spiral 
* Irregular

#### Elliptical

Elliptical galaxies are shaped like a spheriod, or elongated sphere. In the sky, where we can only see two of their three dimensions, these galaxies look like elliptical, or oval, shaped disks. The light is smooth, with the surface brightness decreasing as you go farther out from the center. Elliptical galaxies are given a classification that corresponds to their elongation from a perfect circle, otherwise known as their ellipticity. The larger the number, the more elliptical the galaxy is. So, for example a galaxy of classification of E0 appears to be perfectly circular, while a classification of E7 is very flattened. The elliptical scale varies from E0 to E7. Elliptical galaxies have no particular axis of rotation.

#### Spiral

Spiral galaxies have three main components: a bulge, disk, and halo (see right). The bulge is a spherical structure found in the center of the galaxy. This feature mostly contains older stars. The disk is made up of dust, gas, and younger stars. The disk forms arm structures. Our Sun is located in an arm of our galaxy, the Milky Way. The halo of a galaxy is a loose, spherical structure located around the bulge and some of the disk. The halo contains old clusters of stars, known as globular clusters.

Spiral galaxies are classified into two groups, ordinary and barred. The ordinary group is designated by S or SA, and the barred group by SB. In normal spirals (as seen at above left) the arms originate directly from the nucleus, or bulge, where in the barred spirals (see right) there is a bar of material that runs through the nucleus that the arms emerge from. Both of these types are given a classification according to how tightly their arms are wound. The classifications are a, b, c, d ... with "a" having the tightest arms. In type "a", the arms are usually not well defined and form almost a circular pattern. Sometimes you will see the classification of a galaxy with two lower case letters. This means that the tightness of the spiral structure is halfway between those two letters.

S0 galaxies are an intermediate type of galaxy between E7 and a "true" spiral Sa. They differ from ellipticals because they have a bulge and a thin disk, but are different from Sa because they have no spiral structure. S0 galaxies are also known as Lenticular galaxies.

#### Irregular

Irregular galaxies have no regular or symmetrical structure. They are divided into two groups, Irr I and IrrII. Irr I type galaxies have HII regions, which are regions of elemental hydrogen gas, and many Population I stars, which are young hot stars. Irr II galaxies simply seem to have large amounts of dust that block most of the light from the stars. All this dust makes is almost impossible to see distinct stars in the galaxy.

