package darkskies.ml;

import com.google.common.collect.ImmutableList;
import darkskies.ml.units.Point;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;

public class KMeans {

    private final List<Point> dataPoints;
    private final int numClusters;
    private List<Point> clusterPoints;
    private int generations = 0;
    private List<List<Point>> buckets;

    public KMeans(int numClusters, List<Point> dataPoints) {
        this.dataPoints = dataPoints;
        this.numClusters = numClusters;
        clusterPoints = initializeClusterPoints(numClusters, dataPoints);
    }

    private List<Point> initializeClusterPoints(int numClusters, List<Point> dataPoints) {
        verifySize(numClusters, dataPoints);
        List<Point> clusterPoints = new ArrayList<Point>();
        clusterPoints.add(dataPoints.get(0));
        int index = 1;
        while (clusterPoints.size() < numClusters) {
            if (!clusterPoints.contains(dataPoints.get(index))) {
                clusterPoints.add(dataPoints.get(index));
            } else {
                index++;
            }
        }
        return clusterPoints;
    }

    private void verifySize(int numClusters, List<Point> dataPoints) {
        if (numClusters > dataPoints.size()) {
            throw new RuntimeException("More clusters requested than data points!");
        }
    }

    public KMeansResultSet sort() {
        buckets = initBuckets();

        placeInBuckets(dataPoints, clusterPoints, buckets);
        for (List<Point> bucket : buckets) {

            for (Point point : bucket) {
                ;

            }
        }
        return new KMeansResultSet(clusterPoints, generations);
    }

    private void placeInBuckets(List<Point> dataPoints, List<Point> clusterPoints, List<List<Point>> buckets) {

        for (Point dataPoint : dataPoints) {

            int bucket = 0;
            Point closestPoint = findClosestPoint(clusterPoints, dataPoint, bucket);
            int index = clusterPoints.indexOf(closestPoint);
            buckets.get(index).add(dataPoint);
        }

    }

    private Point findClosestPoint(List<Point> clusterPoints, Point dataPoint, int bucket) {
        Point closestPoint = clusterPoints.get(bucket);

        for (int point = 1; point < clusterPoints.size(); point++) {

            Point clusterPoint = clusterPoints.get(point);
            if (isClusterPointCloser(clusterPoint, closestPoint, dataPoint)) {
                closestPoint = clusterPoint;
            } else {
                int x = 0;
            }
        }
        return closestPoint;
    }

    private boolean isClusterPointCloser(Point clusterPoint, Point closestPoint, Point dataPoint) {
        return abs(closestPoint.distance(dataPoint)) > abs(clusterPoint.distance(dataPoint));
    }

    private List<List<Point>> initBuckets() {
        List<List<Point>> buckets = new ArrayList<List<Point>>();
        for (int i = 0; i < numClusters; i++) {
            buckets.add(new ArrayList<Point>());
        }
        return buckets;
    }

    public class KMeansResultSet {
        private final ImmutableList<Point> clusterCentres;

        public int getGenerations() {
            return generations;
        }

        private final int generations;

        public KMeansResultSet(List<Point> clusterCentres, int generations) {
            this.clusterCentres = new ImmutableList.Builder<Point>().addAll(clusterCentres).build();
            this.generations = generations;
        }


        public ImmutableList<Point> getClusterCentres() {
            return clusterCentres;
        }

     }
}
