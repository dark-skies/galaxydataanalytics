package darkskies.ml.units;

public interface Point<N> {
    float distance(Point<N> p);
    N getX();
    N getY();
    N getZ();
    N getT();
//    N minus(Point p);
    Point<N> add(Point<N> p);
}
