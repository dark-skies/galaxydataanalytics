package darkskies.ml.units;

public class TwoDiminsionsPoint<N> implements Point<N> {

    private final N y;
    private final N x;

    public TwoDiminsionsPoint(N x, N y) {
        this.x = x;
        this.y = y;
    }

    public float distance(Point p) {
        return (Float) this.x - (Float) p.getX();
    }

    public N getX() {
        return null;
    }

    public N getY() {
        return null;
    }

    public N getZ() {
        return null;
    }

    public N getT() {
        return null;
    }

    public Point<N> add(Point<N> p) {
        return null;
    }
}
