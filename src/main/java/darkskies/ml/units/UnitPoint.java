package darkskies.ml.units;

public class UnitPoint<N> implements Point<N> {

    private final N x;

    public UnitPoint(N x) {
        this.x = x;
    }

    public float distance(Point p) {
        return Float.valueOf(this.x.toString()) - Float.valueOf(p.getX().toString());
    }

    public N getX() {
        return x;
    }

    public N getY() {
        return null;
    }

    public N getZ() {
        return null;
    }

    public N getT() {
        return null;
    }

    public Point<N> add(Point<N> p) {
        return (Point<N>) new UnitPoint<Float>(Float.valueOf(
                this.x.toString()) - Float.valueOf(p.getX().toString()));
    }
}
