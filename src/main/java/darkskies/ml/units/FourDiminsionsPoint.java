package darkskies.ml.units;

public class FourDiminsionsPoint<N> implements Point<N> {

    private final N y;
    private final N x;
    private final N z;
    private final N t;

    public FourDiminsionsPoint(N x, N y, N z, N t) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.t = t;
    }

    public float distance(Point p) {
        return (Float) this.x - (Float) p.getX();
    }

    public N getX() {
        return null;
    }

    public N getY() {
        return null;
    }

    public N getZ() {
        return null;
    }

    public N getT() {
        return null;
    }

    public Point<N> add(Point<N> p) {
        return null;
    }
}
