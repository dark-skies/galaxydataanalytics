package darkskies.imaging;

public class Pixel {
    public final int alpha;
    public final int red;
    public final int green;
    public final int blue;
    public final int argb;
    public final int average;
    public final int rgb;

    public Pixel(int argb) {
        this.argb = argb;
        alpha = (argb >> 24) & 0xff;
        red = (argb >> 16) & 0xff;
        green = (argb >> 8) & 0xff;
        blue = (argb) & 0xff;
        average = average();
        rgb = rgb(red, green, blue);
    }


    /*
    int alpha = 0xFF & (argb >> 24);
    int red = 0xFF & ( argb >> 16);
    int green = 0xFF & (argb >> 8 );
    int blue = 0xFF & (argb >> 0 );
     */

    public Pixel(int alpha, int red, int green, int blue) {
        this.alpha = constrain(alpha);
        this.red = constrain(red);
        this.green = constrain(green);
        this.blue = constrain(blue);
        argb = argb(this.alpha, this.red, this.green, this.blue);
        average = average();
        rgb = rgb(this.red, this.green, this.blue);
    }

    private int constrain(int value) {
        if (value < 0) return 0;
        if (value > 255) return 255;
        return value;
    }

    public int grey() {
        return argb(alpha, average, average, average);
    }

    private int average() {
        return (red + green + blue) / 3;
    }

    private int argb(int alpha, int red, int green, int blue) {
        return  ((alpha) << 24) |
                ((red) << 16) |
                ((green) << 8) |
                blue;
    }

    private int rgb(int red, int green, int blue) {
        return  ((red) << 16) |
                ((green) << 8) |
                (blue);
    }

    @Override
    public String toString() {
        return "{\n" +
                "\t\"argb\" : " + argb + ",\n" +
                "\t\"alpha\" : " + alpha + ",\n" +
                "\t\"red\" : " + red + ",\n" +
                "\t\"green\" : " + green + ",\n" +
                "\t\"blue\" : " + blue + ",\n" +
                "\t\"average\" : " + average + "\n" +
                "}";
    }
}