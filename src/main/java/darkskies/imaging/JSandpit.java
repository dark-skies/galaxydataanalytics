package darkskies.imaging;

public class JSandpit {

    public static final int ARGB_255_0_0_0 = -16777216;

    /*
            black (0,0) : {
            "argb" : -16579837,
            "alpha" : 255,
            "red" : 3,
            "green" : 3,
            "blue" : 3,
            "average" : 3
        }
        black (0,2) : {
            "argb" : -7171438,
            "alpha" : 255,
            "red" : 146,
            "green" : 146,
            "blue" : 146,
            "average" : 146
        }
        grey (0,1) : {
            "argb" : -7500403,
            "alpha" : 255,
            "red" : 141,
            "green" : 141,
            "blue" : 141,
            "average" : 141
        }
        white (1,1) : {
            "argb" : -1,
            "alpha" : 255,
            "red" : 255,
            "green" : 255,
            "blue" : 255,
            "average" : 255
        }
             */
    public static void run(int argb) {
        int alpha;
        int red;
        int green;
        int blue;

        alpha = (argb >> 24) & 0xFF;
        red = (argb >> 16)  & 0xFF;
        green = (argb >> 8) & 0xFF;
        blue = argb  & 0xFF;


        System.out.println(alpha);
        System.out.println(red);
        System.out.println(green);
        System.out.println(blue);

        int argbCheck = alpha << 24 | red << 16 | green << 8 | blue;
        System.out.println(String.format("Should be %d and is %d... %b", argb, argbCheck, argb == argbCheck));

    }

    public static void main(String[] args) {
        JSandpit.run(ARGB_255_0_0_0);
    }
}
