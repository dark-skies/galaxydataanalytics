package darkskies.imaging.utils;

import darkskies.imaging.Pixel;

import java.awt.image.BufferedImage;

public class ImageUtils {
    public static int findMaxAverage(BufferedImage image) {
        int maxValue = 0;
        for (int height = 0; height < image.getHeight(); height++) {
            for (int width = 0; width < image.getWidth(); width++) {
                Pixel pixel = new Pixel(image.getRGB(width, height));
                maxValue = pixel.average > maxValue ? pixel.average : maxValue;
            }
        }
        return maxValue;
    }
}
