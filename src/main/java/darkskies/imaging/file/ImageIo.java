package darkskies.imaging.file;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ImageIo {

    public static final String OUTPUT_FORMAT = "png";

    public static BufferedImage load(String source) throws IOException {
        try {
            return ImageIO.read(new File(source));
        } catch (IOException e) {
            System.out.printf("ERROR: Attempted to read file: %s%n", source);
            throw e;
        }
    }

    public static void writeUnformatted(String source, BufferedImage filteredImage) throws IOException {
        Filename filename = new Filename(source);
        ImageIO.write(filteredImage, OUTPUT_FORMAT, new File(
                String.format(
                        "%s/%s.%s",
                        filename.path,
                        filename.basename,
                        OUTPUT_FORMAT)));
    }

    public static void write(String source, BufferedImage filteredImage) throws IOException {

//        TODO: check path can be written to!
        Filename filename = new Filename(source);
        final File file = new File(
                String.format(
                        "%s/altered/%s/%s.altered.%s.%s",
                        filename.path,
                        filename.basename,
                        filename.basename,
                        getDateForFileName(),
                        OUTPUT_FORMAT));
        file.mkdirs();
        ImageIO.write(filteredImage, OUTPUT_FORMAT, file);
    }

    private static String getDateForFileName() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss.SSS");
        return dateFormat.format(new Date());
    }

}
