package darkskies.imaging.file;

import org.apache.commons.io.FilenameUtils;

public class Filename {
    public final String ext;
    public final String basename;
    public final String path;

    public Filename(String source) {
        ext = FilenameUtils.getExtension(source);
        basename = FilenameUtils.getBaseName(source);
        path = FilenameUtils.getPath(source);
    }
}
