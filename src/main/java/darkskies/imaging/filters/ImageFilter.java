package darkskies.imaging.filters;

import java.awt.image.BufferedImage;

public interface ImageFilter {
    BufferedImage filter(final BufferedImage image);
    void setNumClusterPoints(int numClusterPoints);
}
