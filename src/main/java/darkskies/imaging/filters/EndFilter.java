package darkskies.imaging.filters;

import java.awt.image.BufferedImage;

public class EndFilter implements ImageFilter {
    public BufferedImage filter(BufferedImage image) {
        return image;
    }

    public void setNumClusterPoints(int numClusterPoints) {
        // No op
    }
}
