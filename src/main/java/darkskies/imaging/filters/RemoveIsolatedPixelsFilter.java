package darkskies.imaging.filters;

import darkskies.imaging.Pixel;

import java.awt.image.BufferedImage;

public class RemoveIsolatedPixelsFilter implements ImageFilter {

    private final ImageFilter imageFilter;
    private final int span;
    private final int minCount;

    public RemoveIsolatedPixelsFilter(ImageFilter imageFilter, int span, int percentage) {
        this.imageFilter = imageFilter;
        this.span = span;
        final int area = ((this.span * 2) + 1) * ((this.span * 2) + 1);
        minCount = (area * percentage) / 100;
        int x = 0;
    }

    public BufferedImage filter(final BufferedImage image) {
        Pixel blank = new Pixel(255, 0, 0, 0);
        BufferedImage filteredImage = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        for (int height = 0; height < image.getHeight(); height++) {
            for (int width = 0; width < image.getWidth(); width++) {
                if (allowPixel(image, width, height)) {
                    filteredImage.setRGB(width, height, image.getRGB(width, height));
                } else {
                    filteredImage.setRGB(width, height, blank.argb);
                }

            }
        }
        return imageFilter.filter(filteredImage);
    }

    public void setNumClusterPoints(int numClusterPoints) {
        imageFilter.setNumClusterPoints(numClusterPoints);
    }

    private boolean allowPixel(BufferedImage image, int width, int height) {
        int xMin = 0, yMin = 0;
        int xMax = image.getWidth();
        int yMax = image.getHeight();

        int count = 0;
        for (int y = -span; y <= span; y++) {
            for (int x = -span; x <= span; x++) {
                if (withinBounds(width, height, xMin, yMin, xMax, yMax, y, x)) {
                    final Pixel tmp = new Pixel(image.getRGB(width + x, height + y));
                    if (tmp.red != 0 && tmp.green != 0 && tmp.blue != 0) {
                        count++;
                    }
                }
            }
        }
        return count > minCount;
    }

    private boolean withinBounds(int width, int height, int xMin, int yMin, int xMax, int yMax, int y, int x) {
        return (xMax > width + x) && (xMin < width + x) && (yMax > height + y) && (yMin < height + y);
    }
}
