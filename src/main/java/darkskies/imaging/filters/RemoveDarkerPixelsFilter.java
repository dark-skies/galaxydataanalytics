package darkskies.imaging.filters;

import darkskies.imaging.Pixel;
import darkskies.imaging.utils.ImageUtils;

import java.awt.image.BufferedImage;

public class RemoveDarkerPixelsFilter implements ImageFilter {

    private final ImageFilter imageFilter;
    private final int percentageCutOff;

    public RemoveDarkerPixelsFilter(ImageFilter imageFilter, int percentageCutOff) {
        this.imageFilter = imageFilter;
        this.percentageCutOff = percentageCutOff;
    }

    public BufferedImage filter(final BufferedImage image) {
        int cutOff = ImageUtils.findMaxAverage(image) * (100 - percentageCutOff) / 100;

        BufferedImage filteredImage = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        for (int height = 0; height < image.getHeight(); height++) {
            for (int width = 0; width < image.getWidth(); width++) {
                Pixel pixel = new Pixel(image.getRGB(width, height));
                filteredImage.setRGB(width, height, (pixel.average > cutOff ? pixel.argb : 0));
            }
        }
        return imageFilter.filter(filteredImage);
    }

    public void setNumClusterPoints(int numClusterPoints) {
        imageFilter.setNumClusterPoints(numClusterPoints);
    }


}
