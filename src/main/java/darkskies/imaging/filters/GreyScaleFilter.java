package darkskies.imaging.filters;

import darkskies.imaging.Pixel;

import java.awt.image.BufferedImage;

public class GreyScaleFilter implements ImageFilter {

    private final ImageFilter imageFilter;

    public GreyScaleFilter(ImageFilter imageFilter) {
        this.imageFilter = imageFilter;
    }

    public BufferedImage filter(final BufferedImage image) {

        BufferedImage filteredImage = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        for (int height = 0; height < image.getHeight(); height++) {
            for (int width = 0; width < image.getWidth(); width++) {
                Pixel pixel = new Pixel(image.getRGB(width, height));
                filteredImage.setRGB(width, height, pixel.grey());
            }
        }
        return imageFilter.filter(filteredImage);
    }

    public void setNumClusterPoints(int numClusterPoints) {
        imageFilter.setNumClusterPoints(numClusterPoints);
    }
}
