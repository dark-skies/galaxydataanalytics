package darkskies.imaging.filters;

import darkskies.imaging.Pixel;
import darkskies.imaging.utils.ImageUtils;

import java.awt.image.BufferedImage;

public class BucketFilter implements ImageFilter {

    private final ImageFilter imageFilter;
    private final int numberOfBuckets;

    public BucketFilter(ImageFilter imageFilter, int numberOfBuckets) {
        this.imageFilter = imageFilter;
        this.numberOfBuckets = numberOfBuckets;
    }

    public BufferedImage filter(BufferedImage image) {
        int bucketSize = ImageUtils.findMaxAverage(image) / numberOfBuckets - 1;

        BufferedImage filteredImage = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        for (int height = 0; height < image.getHeight(); height++) {
            for (int width = 0; width < image.getWidth(); width++) {

                final Pixel origPixel = new Pixel(image.getRGB(width, height));

                Pixel newPixel = new Pixel(origPixel.alpha,
                        resizePixelComponentToBucket(origPixel.red, bucketSize),
                        resizePixelComponentToBucket(origPixel.green, bucketSize),
                        resizePixelComponentToBucket(origPixel.blue, bucketSize));

                filteredImage.setRGB(width, height, newPixel.argb);
            }
        }

        return imageFilter.filter(filteredImage);
    }

    public void setNumClusterPoints(int numClusterPoints) {
        imageFilter.setNumClusterPoints(numClusterPoints);
    }

    private int resizePixelComponentToBucket(int pixelComponent, int bucketSize) {
        int redBucket = pixelComponent / bucketSize;
        return (redBucket - 1) * bucketSize;
    }
}
