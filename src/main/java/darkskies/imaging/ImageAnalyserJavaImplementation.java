package darkskies.imaging;

import darkskies.imaging.file.ImageIo;
import darkskies.imaging.filters.*;

import java.awt.image.BufferedImage;
import java.io.IOException;

public class ImageAnalyserJavaImplementation {

    private final ImageFilter imageFilter;

    public ImageAnalyserJavaImplementation(ImageFilter imageFilter) {
        this.imageFilter = imageFilter;
    }


    public void process(String source) {

        try {
            final BufferedImage image = ImageIo.load(source);
            BufferedImage filteredImage = imageFilter.filter(image);
            ImageIo.write(source, filteredImage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        EndFilter endFilter = new EndFilter();
        ImageFilter removeIsolatedPixelsFilter1 = new RemoveIsolatedPixelsFilter(endFilter, 2, 85);
        ImageFilter removeIsolatedPixelsFilter2 = new RemoveIsolatedPixelsFilter(removeIsolatedPixelsFilter1, 3, 85);
        ImageFilter removeDarkerPixelsFilter = new RemoveDarkerPixelsFilter(removeIsolatedPixelsFilter2, 60);
        ImageFilter greyScaleFilter = new GreyScaleFilter(removeDarkerPixelsFilter);
        ImageAnalyserJavaImplementation imageAnalyser = new ImageAnalyserJavaImplementation(greyScaleFilter);
        imageAnalyser.process("./data/images/ab2218.jpg");
    }
}
