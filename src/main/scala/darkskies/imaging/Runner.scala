package darkskies.imaging

import darkskies.imaging.flows.FlowOne

object Runner {
    val CRAB: String = "./data/images/crab.jpg"
    val M31: String = "./data/images/m31.jpg"
    val M83: String = "./data/images/m83.jpg"
    val AB2218: String = "./data/images/ab2218.jpg"

    def main(args: Array[String]) {
        FlowOne.process(CRAB)
    }
}
