package darkskies.imaging

import javafx.geometry.Orientation

import darkskies.imaging.filters._
import org.apache.spark.{SparkContext, SparkConf}

object KMeansImageAnalyser {

    def get(iterations: Int = 5) : ImageAnalyser = {
        val sconf : SparkConf = new SparkConf().setAppName("Detect Galactic centres").setMaster("local")
        val sc: SparkContext = new SparkContext(sconf)
        val kMeansFilter = new KmeansFilter(new EndFilter, sc, iterations, 5)
        val bucketFilter = new BucketFilter(kMeansFilter, 3)
        val greyScaleFilter = new GreyScaleFilter(bucketFilter)
        val combineInputAndOutputFilter = new CombineInputAndOutputFilter(greyScaleFilter, Orientation.VERTICAL)
        new ImageAnalyser(combineInputAndOutputFilter)
    }
}
