package darkskies.imaging

import darkskies.imaging.file.ImageIo
import darkskies.imaging.filters.ImageFilter

class ImageAnalyser(imageFilter: ImageFilter) {

    def process(source: String, numClusterPoints: Int): Unit = {
        val image = ImageIo.load(source)
        imageFilter.setNumClusterPoints(numClusterPoints)
        val filteredImage = imageFilter.filter(image)
        ImageIo.write(source, filteredImage)
    }
}
