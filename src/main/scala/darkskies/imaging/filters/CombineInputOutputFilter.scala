package darkskies.imaging.filters

import java.awt.image.BufferedImage
import javafx.geometry.Orientation

import darkskies.imaging.Pixel

class CombineInputAndOutputFilter(imageFilter: ImageFilter, orientation: Orientation) extends ImageFilter {
    val separatorWidth: Int = 5
    val white: Int = new Pixel(255, 255, 255, 255).argb

    override def filter(image: BufferedImage): BufferedImage = {
        createCombinedCanvas(image)
        val filteredImage = imageFilter.filter(image)
        combine(image, filteredImage)
    }

    override def setNumClusterPoints(numClusterPoints: Int): Unit = {
        imageFilter.setNumClusterPoints(numClusterPoints)
    }


    def combine(image: BufferedImage, filteredImage: BufferedImage): BufferedImage = {
        val combined: BufferedImage = createCombinedCanvas(image)
        insertImage(image, combined, 0 ,0)
        insertSeparator(image, combined)

        val verticalAdjustment: Int = if (orientation == Orientation.VERTICAL) image.getHeight + separatorWidth else 0
        val horizontalAdjustment: Int = if (orientation == Orientation.HORIZONTAL) image.getWidth + separatorWidth else 0

        insertImage(filteredImage, combined, verticalAdjustment ,horizontalAdjustment)

        combined
    }


    def createCombinedCanvas(original: BufferedImage): BufferedImage = {
        if (orientation == Orientation.HORIZONTAL) {
            new BufferedImage((original.getWidth * 2) + separatorWidth, original.getHeight, original.getType)
        } else {
            new BufferedImage(original.getWidth, (original.getHeight * 2) + separatorWidth, original.getType)
        }
    }

    def insertImage(image: BufferedImage, canvas: BufferedImage, verticalAdjustment: Int, horizontalAdjustment: Int): Unit = {
        for (x <- 0 until image.getWidth) {
            for (y <- 0 until image.getHeight) {
                canvas.setRGB(x + horizontalAdjustment, y + verticalAdjustment, image.getRGB(x, y))
            }
        }
    }

    def insertSeparator(original: BufferedImage, combined: BufferedImage): Unit = {
        if (orientation == Orientation.HORIZONTAL)
            writeSeparator(combined, original.getWidth, original.getWidth + separatorWidth, 0, original.getHeight)
        else
            writeSeparator(combined, 0, original.getWidth, original.getHeight, original.getHeight + separatorWidth)
    }

    def writeSeparator(combined: BufferedImage, xStart: Int, xEnd: Int, yStart: Int, yEnd: Int): Unit = {
        for (x <- xStart until xEnd) {
            for (y <- yStart until yEnd) {
                combined.setRGB(x, y, white)
            }
        }
    }
}