package darkskies.imaging.filters

import java.awt.image.BufferedImage

import darkskies.imaging.Pixel
import org.apache.spark.SparkContext
import org.apache.spark.mllib.clustering.KMeans
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.linalg.{Vectors, Vector}

class KmeansFilter(imageFilter: ImageFilter,
                   sc: SparkContext,
                   numIterations: Int = 20,
                   numBuckets: Int) extends ImageFilter {

    private val alphaMask: Int = 255
    private val red: Int = new Pixel(alphaMask, 255, 0, 0).argb
    private val black: Int = new Pixel(alphaMask, 0, 0, 0).argb
    private var numClusters: Int = 2;

    override def filter(image: BufferedImage): BufferedImage = {

        val strengthenedSeqVector = convertToSeqVector(image)

        val rddVector: RDD[Vector] = sc.parallelize(strengthenedSeqVector)

        val clusters = KMeans.train(rddVector, numClusters, numIterations)

        val centers = clusters.clusterCenters

        centers.foreach {
            center => setCrosshairs(image, center)
        }

        image
    }

    def innerIter(x: Int, y: Int, pix: Int): Seq[Vector] = {
        val pixelAverage: Int = new Pixel(pix).average
        val str: Int = pixelAverage / (255 / numBuckets)
        for {
            s <- 0 to str

        } yield Vectors.dense(x, y)
    }

    def convertToSeqVector(image: BufferedImage) : Seq[Vector] = {
        (
            for {
                i <- 0 until image.getHeight * image.getWidth
                x: Int = i % image.getWidth
                y: Int = i / image.getWidth
                if image.getRGB(x, y) > black
            }  yield innerIter(x, y, image.getRGB(x, y))
        ).flatten
    }

    def setCrosshairs(image: BufferedImage, center: Vector): Unit = {
        val width = image.getWidth
        val height = image.getHeight
        val centralX: Int = Math.round(center(0)).toInt
        val centralY: Int = Math.round(center(1)).toInt

        println("center: X: " + centralX + "   Y: " + centralY)

        for (x <- (if (centralX - 5 < 0) 0 else centralX - 5) until
                (if (centralX + 5 > width) width else centralX + 5)) {
            image.setRGB(
                x,
                centralY,
                red)
        }

        for (y <- (if (centralY - 5 < 0) 0 else centralY - 5) until
                (if (centralY + 5 > width) width else centralY + 5)) {
            image.setRGB(
                centralX,
                y,
                red)
        }
    }

    override def setNumClusterPoints(numClusterPoints: Int): Unit = {
        numClusters = numClusterPoints
        imageFilter.setNumClusterPoints(numClusterPoints)
    }
}
