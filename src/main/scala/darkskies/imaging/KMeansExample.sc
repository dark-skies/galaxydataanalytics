package darkskies.imaging

import org.apache.spark.mllib.clustering.KMeans
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.{SparkConf, SparkContext}

object KMeansExample {

    def run(): Unit = {

        val conf = new SparkConf().setAppName("Kmeans").setMaster("local")
        val sc = new SparkContext(conf)

        val dataSource: String = "./data/mllib/kmeans_data.txt"
        // Load and parse the data
        val data = sc.textFile(dataSource)
        val parsedData = data.map(s => Vectors.dense(s.split(' ').map(_.toDouble))).cache()

        // Cluster the data into two classes using KMeans
        val numClusters = 2
        val numIterations = 20
        val clusters = KMeans.train(parsedData, numClusters, numIterations)

        // Evaluate clustering by computing Within Set Sum of Squared Errors
        val WSSSE = clusters.computeCost(parsedData)
        println("Within Set Sum of Squared Errors = " + WSSSE)
    }

    def main(args: Array[String]) {
        KMeansExample.run()
    }
}
