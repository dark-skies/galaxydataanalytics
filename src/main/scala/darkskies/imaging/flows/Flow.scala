package darkskies.imaging.flows

trait Flow {

    def process(source: String)
}
