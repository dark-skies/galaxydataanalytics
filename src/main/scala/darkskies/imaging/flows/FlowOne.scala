package darkskies.imaging.flows

import darkskies.imaging.ImageAnalyser
import darkskies.imaging.filters.{GreyScaleFilter, RemoveDarkerPixelsFilter, EndFilter}

object FlowOne extends Flow {

    override def process(source: String): Unit = {
        val endFilter = new EndFilter()
        val removeDarkerPixelsFilter = new RemoveDarkerPixelsFilter(endFilter, 20)
        val greyScaleFilter = new GreyScaleFilter(removeDarkerPixelsFilter)

        new ImageAnalyser(greyScaleFilter)
    }
}
