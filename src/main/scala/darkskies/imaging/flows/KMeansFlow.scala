package darkskies.imaging.flows

import darkskies.imaging.ImageAnalyser
import darkskies.imaging.filters.{EndFilter, GreyScaleFilter, RemoveDarkerPixelsFilter}
import org.apache.spark.{SparkConf, SparkContext}

object KMeansFlow extends Flow {

    override def process(source: String): Unit = {

        val sparkConf = new SparkConf().setAppName("KmeansFlow").setMaster("local")
        val sparkContext = new SparkContext(sparkConf)
        val endFilter = new EndFilter()
//        val kmeansFilter = new KmeansFilter(endFilter, sparkContext)
        val removeDarkerPixelsFilter = new RemoveDarkerPixelsFilter(endFilter, 20)
        val greyScaleFilter = new GreyScaleFilter(removeDarkerPixelsFilter)

        new ImageAnalyser(greyScaleFilter)
    }
}
