package darkskies.imaging

object Listing {

    def byFourOnly(n: Int): Boolean = {
        n % 4 == 0
    }

    def isEven(n: Int): Boolean = {
        n % 2 == 0
    }

    def question(list: List[Int]): Unit = {
        list.filter(isEven).filter(byFourOnly).foreach(println)
//        filter isEven foreach println
    }

    def main(args: Array[String]) {
        Listing.question(List(1, 2, 3, 4, 5 ,6, 7, 8, 9, 10))
    }
}
