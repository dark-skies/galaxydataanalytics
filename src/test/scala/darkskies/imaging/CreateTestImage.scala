package darkskies.imaging

import java.awt.image.BufferedImage
import java.awt.image.BufferedImage._

object CreateTestImage {
    val alphaMask: Int = 255
    val black: Int = new Pixel(alphaMask, 0, 0, 0).argb
    val grey: Int = new Pixel(alphaMask, 128, 128, 128).argb
    val white: Int = new Pixel(alphaMask, 255, 255, 255).argb

    def createTestImage(width: Int, height: Int, coords : List[(Int, Int)]) : BufferedImage = {
        val image = new BufferedImage(width, height, TYPE_INT_RGB)

        setBackgroundColour(width, height, image)

        coords foreach { coord =>
            if (withinBounds(coord._1, coord._2, width, height)) {
                image.setRGB(coord._1, coord._2, white)
            }
            for (x <- coord._1 - 1 to coord._1 + 1) {
                for (y <- coord._2 - 1 to coord._2 + 1) {
                    if (withinBounds(x, y, width, height)) {
                        if (image.getRGB(x, y) < grey) {
                            image.setRGB(x, y, grey)
                        }
                    }
                }
            }
        }
        image
    }

    def withinBounds(x: Int, y: Int, width: Int, height: Int): Boolean = {
        x >= 0 && x < width && y >= 0 && y < height
    }

    def setBackgroundColour(width: Int, height: Int, image: BufferedImage): Unit = {
        for (x <- 0 until width) {
            for (y <- 0 until height) {
                image.setRGB(x, y, black)
            }
        }
    }
}
