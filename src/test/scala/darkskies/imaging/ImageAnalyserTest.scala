package darkskies.imaging

import java.awt.image.BufferedImage

import darkskies.imaging.file.ImageIo
import darkskies.imaging.filters._
import org.apache.commons.lang.time.StopWatch
import org.apache.spark.{SparkConf, SparkContext}
import org.scalatest.{BeforeAndAfter, FunSuite}

class ImageAnalyserTest extends FunSuite with BeforeAndAfter {

    val BASE = "./data/images/"
    val NGC_4038_NGC_4039 = BASE + "NGC-4038-NGC-4039.jpg"
    val MOON = BASE + "moon.jpg"
    val M31 = BASE + "m31.jpg"
    val M83 = BASE + "m83.jpg"
    val ARP271 = BASE + "arp271.jpg"
    val CRAB = BASE + "crab.jpg"
    val TEST = BASE + "test.png"
    val ARP87 = BASE + "arp87.jpg"
    val HL_TAU = BASE + "hl-tau.jpg"

    test("can load and debug the alpha red green blue values") {

        val bi = ImageIo.load("./data/images/sample.jpg")


        println("black (0,0) : " + new Pixel(bi.getRGB(0, 0)))
        println("black (0,2) : " + new Pixel(bi.getRGB(2, 0)))
        println("grey (0,1) : " + new Pixel(bi.getRGB(1, 0)))
        println("white (1,1) : " + new Pixel(bi.getRGB(1, 1)))

        assert(120 === 1)

    }

    test("The KMeans flow") {
        val sconf : SparkConf = new SparkConf().setAppName("Detect Galactic centres").setMaster("local")
        val sc: SparkContext = new SparkContext(sconf)

        val imageSourceFile: String = TEST
        val testImage: BufferedImage = CreateTestImage.createTestImage(50, 50, List((0, 0), (10, 1), (11, 21)))
        ImageIo.writeUnformatted(imageSourceFile, testImage)

        val kMeansFilter = new KmeansFilter(new EndFilter, sc, 10, 6)
        val greyScaleFilter = new GreyScaleFilter(kMeansFilter)
        val ia = new ImageAnalyser(greyScaleFilter)
        ia.process(imageSourceFile, 3)

    }

    test("The KMeans flow against real image") {
        val ia = KMeansImageAnalyser.get()
        val stopwatch : StopWatch = new StopWatch()
        stopwatch.start()
        val startTime: Long = stopwatch.getTime
        println("Start: " + startTime)

        val imageSourceFile: String = ARP87

        ia.process(imageSourceFile, 2)
        val endTime: Long = stopwatch.getTime
        println("End: " + endTime)
        println("time: " + (endTime - startTime))
    }

    test("The KMeans flow all real images") {
        val ia = KMeansImageAnalyser.get()
        val stopwatch : StopWatch = new StopWatch()
        stopwatch.start()
        val startTime: Long = stopwatch.getTime
        println("Start: " + startTime)
        val images: List[(String, Int)] = List(
                (ARP271, 2), (ARP87, 2), (CRAB, 1),
                (HL_TAU, 1), (M31, 1), (M83, 1),
                (NGC_4038_NGC_4039, 2))

        images.foreach(i => ia.process(i._1, i._2))

        val endTime: Long = stopwatch.getTime
        println("End: " + endTime)
        println("time: " + (endTime - startTime))
    }

    test("creating test image!") {
        ImageIo.writeUnformatted("./data/images/testCreateImage.png", CreateTestImage.createTestImage(5, 5, List((0, 0), (2, 2), (4, 4), (5, 4))))
    }
}
