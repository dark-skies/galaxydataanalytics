package darkskies.imaging;

import org.apache.commons.io.FilenameUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FilenameTest {

    @Test
    public void check_filenames() throws Exception {
        String source = "./data/images/crab.jpg";
        String ext = FilenameUtils.getExtension(source);
        String basename = FilenameUtils.getBaseName(source);
        String path = FilenameUtils.getPath(source);
        assertEquals("jpg", ext);
        assertEquals("crab", basename);
        assertEquals("./data/images/", path);
    }
}