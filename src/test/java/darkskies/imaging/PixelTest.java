package darkskies.imaging;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PixelTest {

    @Test
    public void can_convert_argb_to_pixel_and_back() throws Exception {
        int argb = -16579837;

        Pixel pixel = new Pixel(argb);

        assertEquals(255, pixel.alpha);
        assertEquals(3, pixel.red);
        assertEquals(3, pixel.green);
        assertEquals(3, pixel.blue);
        assertEquals(3, pixel.average);

        assertEquals(argb, pixel.argb);
    }
}