package darkskies.imaging;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import darkskies.ml.KMeans;
import darkskies.ml.units.Point;
import darkskies.ml.units.UnitPoint;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class KMeansTest {

    @Test
    public void can_use_integers() throws Exception {
        List<Point> dataPoints = getList(Lists.newArrayList(9, 6, 5, 5, 3, 3, 2, 1, 1, 1, 1));
        KMeans kMeans = new KMeans(3, dataPoints);

        KMeans.KMeansResultSet resultSet = kMeans.sort();

        ImmutableList clusterCentres = resultSet.getClusterCentres();
        assertEquals(3, clusterCentres.size());
        assertTrue(dataPoints.contains(clusterCentres.get(0)));
        assertTrue(dataPoints.contains(clusterCentres.get(1)));
        assertTrue(dataPoints.contains(clusterCentres.get(2)));
    }

    private List<Point> getList(ArrayList<Integer> integers) {
        List<Point> points = new ArrayList<Point>();
        for (Integer i : integers) {
            points.add(new UnitPoint(i));
        }
        return points;
    }
}